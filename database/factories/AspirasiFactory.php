<?php

namespace Database\Factories;

use App\Models\Aspirasi;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Aspirasi>
 */
class AspirasiFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */

     protected $model = Aspirasi::class;

    public function definition(): array
    {
        return [
            'judul' => $this->faker->sentence(2),
            'nis' => $this->faker->numerify('539#######'),
            'aspirasi_id' => $this->faker->numerify('#######'),
            'kategori_id' => $this->faker->numberBetween(1, 3),
            'tujuan_id' => $this->faker->numberBetween(1, 5)
        ];
    }
}
