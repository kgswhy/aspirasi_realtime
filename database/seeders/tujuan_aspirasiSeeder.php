<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class tujuan_aspirasiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        \App\Models\tujuan_aspirasi::create([
            'id_tujuan'	=> 1,
            'tujuan'	=> 'Hubungan Industri'
        ]);

        \App\Models\tujuan_aspirasi::create([
            'id_tujuan'	=> 2,
            'tujuan'	=> 'Kesiwaan'
        ]);

        \App\Models\tujuan_aspirasi::create([
            'id_tujuan'	=> 3,
            'tujuan'	=> 'Sarana dan Prasarana'
        ]);

        \App\Models\tujuan_aspirasi::create([
            'id_tujuan'	=> 4,
            'tujuan'	=> 'Kurikulum'
        ]);

        \App\Models\tujuan_aspirasi::create([
            'id_tujuan'	=> 5,
            'tujuan'	=> 'BK'
        ]);
    }
}
