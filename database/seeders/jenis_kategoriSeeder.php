<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class jenis_kategoriSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        \App\Models\jenis_kategori::create([
                'id_kategori'	=> 1,
                'jenis_kategori'	=> 'Pengaduan'
        ]);

        \App\Models\jenis_kategori::create([
                'id_kategori'	=> 2,
                'jenis_kategori'	=> 'Aspirasi'
        ]);

        \App\Models\jenis_kategori::create([
                'id_kategori'	=> 3,
                'jenis_kategori'	=> 'Permintaan Info'
        ]);
    }
}
