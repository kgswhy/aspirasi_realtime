@extends('Frontend.home.user.Layout.template')

@section('page-title')
    Kirim Aspirasi - Aspire
@endsection


@section('content')
    <div class="mx-auto max-w-screen-xl px-4 py-16 sm:px-6 lg:px-8">
        <div class="mx-auto max-w-lg z-0">
            <h1 class="text-center text-2xl font-bold text-white sm:text-3xl"> Kirim Aspirasi anda </h1>
            <p class="mx-auto mt-4 max-w-md text-center text-gray-500"> Aspirasi anda diperlukan untuk perkembangan yang
                dinanti </p>
            <div class="rounded-lg bg-dark p-8 shadow-lg lg:col-span-3 lg:p-12">
                <form class="space-y-4 aspirasi-form" enctype="multipart/form-data">
                    @csrf
                    <label class="block mb-2 text-sm font-medium text-gray-900 dark:text-white" for="email">Jenis
                        Aspirasi</label>
                    <div class="grid grid-cols-1 gap-4 text-center sm:grid-cols-3">

                        @foreach ($jenis_kategori as $j)
						<div class="rounded-lg bg-dark shadow-lg lg:col-span-1 ">
                            <div>
                                <input class="peer sr-only" value="{{ $j['id_kategori'] }}"
                                    id="aspirasi_{{ $j['id_kategori'] }}" type="radio" tabindex="-1"
                                    name="jenis_aspirasi" />
                                <label for="aspirasi_{{ $j['id_kategori'] }}"
                                    class="block w-full rounded-lg border p-3 peer-checked:border-black peer-checked:bg-blue-600 "
                                    tabindex="0">
                                    <span class="text-sm font-medium"> {{ $j['jenis_kategori'] }} </span>
                                </label>
                            </div>
						</div>
                        @endforeach
                    </div>
                    <div>
                        <label class="block mb-2 text-sm font-medium text-gray-900 dark:text-white"
                            for="name">Judul</label>
                        <input
                            class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                            placeholder="Judul" type="text" name="judul_aspirasi" />
                    </div>
                    <label for="countries"
                        class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Category</label>

                    <select name="tujuan_aspirasi" id="countries"
                        class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-black focus:border-black block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
                        <option selected disabled>Choose</option>
                        @foreach ($tujuan_aspirasi as $ta)
                            <option value="{{ $ta['id_tujuan'] }}">{{ $ta['tujuan'] }}</option>
                        @endforeach
                    </select>
                    <label class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Upload foto</label>
                    <input
                        class="block w-full text-x text-gray-900 border border-gray-300 rounded-lg cursor-pointer bg-gray-50 dark:text-gray-400 focus:outline-none dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400"
                        name="bukti" type="file" />
                    <label for="message" class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Your
                        message</label>
                    <textarea name="pesan_aspirasi" rows="4"
                        class="block p-2.5 w-full text-sm text-gray-900 bg-gray-50 rounded-lg border border-gray-300 focus:ring-blue-500 focus:border-blue-500 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                        placeholder="Leave a comment..."></textarea>
                    <div class="flex items-center mb-4">
                        <input checked name="tnc" type="checkbox" value=""
                            class="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 dark:focus:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600" />
                        <label for="checkbox-1" class="ml-2 text-sm font-medium text-gray-900 dark:text-gray-300"> Saya
                            bertanggung jawab atas aspirasi yang saya beriksa <a data-modal-target="defaultModal"
                                data-modal-toggle="defaultModal"
                                class="text-blue-600 hover:underline dark:text-blue-500">sesuai dengan persyaratan dan
                                perjanjian yang ada</a>. </label>
                        <div id="defaultModal" tabindex="-1" aria-hidden="true"
                            class="fixed top-0 left-0 right-0 z-50 hidden w-full p-4 overflow-x-hidden overflow-y-auto md:inset-0 h-modal md:h-full">
                            <div class="relative w-full h-full max-w-2xl md:h-auto">
                                <!-- Modal content -->
                                <div class="relative bg-white rounded-lg shadow dark:bg-gray-700">
                                    <!-- Modal header -->
                                    <div
                                        class="flex items-start justify-between p-4 border-b rounded-t dark:border-gray-600">
                                        <h3 class="text-xl font-semibold text-gray-900 dark:text-white"> Persyaratan dan
                                            Perjanjian Penggunaan Layanan </h3>
                                        <button type="button"
                                            class="text-gray-400 bg-transparent hover:bg-gray-200 hover:text-gray-900 rounded-lg text-sm p-1.5 ml-auto inline-flex items-center dark:hover:bg-gray-600 dark:hover:text-white"
                                            data-modal-hide="defaultModal">
                                            <svg aria-hidden="true" class="w-5 h-5" fill="currentColor"
                                                viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                                                <path fill-rule="evenodd"
                                                    d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
                                                    clip-rule="evenodd"></path>
                                            </svg>
                                            <span class="sr-only">Close modal</span>
                                        </button>
                                    </div>
                                    <!-- Modal body -->
                                    <div class="p-6 space-y-6">
                                        <p class="text-base leading-relaxed text-gray-500 dark:text-gray-400"> Persyaratan
                                            penggunaan layanan yang harus dipatuhi adalah pengguna wajib memberikan
                                            informasi yang benar dan lengkap ketika melakukan pendaftaran untuk menggunakan
                                            layanan ini. </p>
                                        <p class="text-base leading-relaxed text-gray-500 dark:text-gray-400"> Hal ini
                                            dilakukan untuk memastikan keamanan dan kualitas layanan yang diberikan. Oleh
                                            karena itu, pengguna diharapkan untuk tidak memberikan informasi palsu atau
                                            menyesatkan dalam proses pendaftaran dan aspirasi. Jika pengguna memberikan
                                            informasi yang tidak akurat atau tidak lengkap, maka pengguna bertanggung jawab
                                            atas segala risiko dan konsekuensi yang mungkin terjadi. </p>
                                    </div>
                                    <!-- Modal footer -->
                                    <div
                                        class="flex items-center p-6 space-x-2 border-t border-gray-200 rounded-b dark:border-gray-600">
                                        <button data-modal-hide="defaultModal" type="button"
                                            class="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">
                                            Saya Setuju </button>
                                        <button data-modal-hide="defaultModal" type="button"
                                            class="text-gray-500 bg-white hover:bg-gray-100 focus:ring-4 focus:outline-none focus:ring-blue-300 rounded-lg border border-gray-200 text-sm font-medium px-5 py-2.5 hover:text-gray-900 focus:z-10 dark:bg-gray-700 dark:text-gray-300 dark:border-gray-500 dark:hover:text-white dark:hover:bg-gray-600 dark:focus:ring-gray-600">
                                            Tidak Setuju </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="mt-4">
                        <button type="button"
                            class="send-aspirasi inline-block w-full rounded-lg bg-blue-600 px-5 py-3 font-medium text-white sm:w-auto">Send
                            Enquiry</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
