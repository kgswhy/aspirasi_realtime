
@if(Auth()->check())
<!-- component -->
<script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.x.x/dist/alpine.min.js"></script>

<div x-data="{ open: false }" class="mx-10 shadow flex justify-center items-center">
    <div @click="open = !open" class="relative border-b-4 border-transparent py-3"
        :class="{ 'border-indigo-700 transform transition duration-300 ': open }"
        x-transition:enter-end="transform opacity-100 scale-100" x-transition:leave="transition ease-in duration-75"
        x-transition:leave-start="transform opacity-100 scale-100">
        <div class="flex justify-center items-center space-x-3 cursor-pointer">
            <div class="w-7 h-7 rounded-full overflow-hidden border-2 dark:border-white border-gray-900">
                <img src="https://images.unsplash.com/photo-1610397095767-84a5b4736cbd?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=750&q=80"
                    alt="" class="w-full h-full object-cover">
            </div>
            <div class="font-semibold dark:text-white text-gray-900 text-xs">
                <div class="cursor-pointer">{{ auth()->user()->name }}</div>
            </div>
        </div>
        <div x-show="open" x-transition:enter="transition ease-out duration-100"
            x-transition:enter-start="transform opacity-0 scale-60"
            x-transition:enter-end="transform opacity-100 scale-100" x-transition:leave="transition ease-in duration-75"
            x-transition:leave-start="transform opacity-100 scale-100"
            x-transition:leave-end="transform opacity-0 scale-95"
            class="absolute w-35 px-5 py-3 dark:bg-gray-800 bg-white rounded-lg shadow border dark:border-transparent mt-5">
            <ul class="space-y-3 dark:text-white">
                <li class="font-medium">
                    <a href="{{ route('Account') }}"
                        class="flex items-center transform transition-colors duration-200 border-r-4 border-transparent">
                        <div class="mr-3">
                            <svg class="w-6 h-3" fill="none" stroke="currentColor" viewBox="0 0 24 24"
                                xmlns="http://www.w3.org/2000/svg">
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                    d="M16 7a4 4 0 11-8 0 4 4 0 018 0zM12 14a7 7 0 00-7 7h14a7 7 0 00-7-7z"></path>
                            </svg>
                        </div>
                        Account
                    </a>
                </li>
                <hr class="dark:border-gray-700">
                <li class="font-medium">
                    <a href="{{ route('logout') }}"
                        class="flex items-center transform transition-colors duration-200 border-r-4 border-transparent hove:underline ">
                        <div class="mr-3 text-red-600">
                            <svg class="w-6 h-6" fill="none" stroke="currentColor" viewBox="0 0 24 24"
                                xmlns="http://www.w3.org/2000/svg">
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                    d="M17 16l4-4m0 0l-4-4m4 4H7m6 4v1a3 3 0 01-3 3H6a3 3 0 01-3-3V7a3 3 0 013-3h4a3 3 0 013 3v1">
                                </path>
                            </svg>
                        </div>
                        Logout
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>
@else

<a class="rounded-lg hover:underline px-5 py-2 text-sm font-medium text-white" href="{{ route('login_get') }}">
    Log in
</a>

@endif