@extends('Frontend.home.user.Layout.template')
@section('page-title')
Aspire - Platform Aspirasi Digital Untuk Sekolah
@endsection

@section('content')
<div class="mx-auto max-w-screen-xl px-4 py-32 lg:flex lg:h-screen lg:items-center">
  <div class="mx-auto max-w-3xl text-center">
      <h1
          class="bg-gradient-to-r from-green-300 via-blue-500 to-purple-600 bg-clip-text text-3xl font-extrabold text-transparent sm:text-5xl">
          Beraspirasi Digital<span class="sm:block"> Berdiskusi Tanpa Batas </span></h1>

      <p class="mx-auto mt-4 max-w-xl sm:text-xl sm:leading-relaxed">
          Mari beralih ke beraspirasi digital! Saatnya untuk merubah cara kita beraspirasi dan berdiskusi dengan
          teknologi digital.
      </p>

      <div class="mt-8 flex flex-wrap justify-center gap-4">
          <a class="block w-full rounded shadow-lg border border-blue-600 bg-blue-600 px-12 py-3 text-sm font-medium text-white hover:bg-transparent hover:text-white focus:outline-none focus:ring active:text-opacity-75 sm:w-auto"
              href="{{ route('aspirasi') }}">Masuk dan Mulai</a>

          <a class="block w-full rounded-lg bg-dark shadow-lg px-12 py-3 text-sm font-medium text-white hover:underline sm:w-auto"
              href="#learn">
              Pelajari Lebih
          </a>

      </div>
  </div>
</div>

<section id="learn" class="text-gray-400 bg-gray-900 body-font">
  <div class="container px-5 py-24 mx-auto flex flex-wrap">
      <div class="flex flex-col text-center w-full mb-20">
          <h1 class="sm:text-3xl text-2xl font-medium title-font mb-4 text-white">Tata Cara Menggunakan Layanan
              Aspirasi Digital Kami</h1>
          <p class="lg:w-2/3 mx-auto leading-relaxed text-base">Kami ingin memastikan bahwa Anda dapat memanfaatkan
              layanan aspirasi digital kami dengan maksimal. Oleh karena itu, berikut adalah tata cara yang perlu Anda
              ketahui untuk menggunakan layanan kami:</p>
      </div>

      <div class="flex relative pt-10 pb-20 sm:items-center md:w-2/3 mx-auto">
          <div class="h-full w-6 absolute inset-0 flex items-center justify-center">

          </div>
          <div class="rounded-lg bg-dark p-8 shadow-lg lg:col-span-3 lg:p-12">
              <div
                  class="flex-shrink-0 w-6 h-6 rounded-full mt-10 sm:mt-0 inline-flex items-center justify-center bg-blue-500 text-white relative z-10 title-font font-medium text-sm">
                  1</div>
              <div class="flex-grow md:pl-8 pl-6 flex sm:items-center items-start flex-col sm:flex-row">
                  <div
                      class="flex-shrink-0 w-24 h-24 bg-gray-800 text-blue-400 rounded-full inline-flex items-center justify-center">
                      <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor"
                          stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="w-12 h-12"">
                          <circle cx="12" cy="12" r="10"></circle>
                          <line x1="2" y1="12" x2="22" y2="12"></line>
                          <path d="M12 2a8 8 0 0 1 8 8c0 4.418-3.582 8-8 8s-8-3.582-8-8 3.582-8 8-8z"></path>
                      </svg>
                  </div>
                  <div class="flex-grow sm:pl-6 mt-6 sm:mt-0">
                      <h2 class="font-medium title-font text-white mb-1 text-xl">Akses web</h2>
                      <p class="leading-relaxed">Akses web yang disediakan oleh pihak yang mengadakan sesi
                          beraspirasi. Biasanya formulir tersebut dapat diakses melalui tautan yang diberikan.</p>
                  </div>
              </div>
          </div>
      </div>
      <div class="flex relative pb-20 sm:items-center md:w-2/3 mx-auto">
          <div class="h-full w-6 absolute inset-0 flex items-center justify-center">

          </div>
          <div class="rounded-lg bg-dark p-8 shadow-lg lg:col-span-3 lg:p-12">
              <div
                  class="flex-shrink-0 w-6 h-6 rounded-full mt-10 sm:mt-0 inline-flex items-center justify-center bg-blue-500 text-white relative z-10 title-font font-medium text-sm">
                  2</div>
              <div class="flex-grow md:pl-8 pl-6 flex sm:items-center items-start flex-col sm:flex-row">
                  <div
                      class="flex-shrink-0 w-24 h-24 bg-gray-800 text-blue-400 rounded-full inline-flex items-center justify-center">
                      <svg fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round"
                          stroke-width="2" class="w-12 h-12" viewBox="0 0 24 24">
                          <path d="M7 2v20M17 2v20M2 7h20M2 17h20"></path>
                      </svg>

                  </div>
                  <div class="flex-grow sm:pl-6 mt-6 sm:mt-0">
                      <h2 class="font-medium title-font text-white mb-1 text-xl">Isi data diri</h2>
                      <p class="leading-relaxed">Isilah data diri yang diminta pada web seperti nama, alamat email,
                          nomor telepon, dan sebagainya. Pastikan data yang diisi benar dan valid.</p>
                  </div>
              </div>
          </div>
      </div>
      <div class="flex relative pb-20 sm:items-center md:w-2/3 mx-auto">
          <div class="h-full w-6 absolute inset-0 flex items-center justify-center">

          </div>
          <div class="rounded-lg bg-dark p-8 shadow-lg lg:col-span-3 lg:p-12">
              <div
                  class="flex-shrink-0 w-6 h-6 rounded-full mt-10 sm:mt-0 inline-flex items-center justify-center bg-blue-500 text-white relative z-10 title-font font-medium text-sm">
                  3</div>
              <div class="flex-grow md:pl-8 pl-6 flex sm:items-center items-start flex-col sm:flex-row">
                  <div
                      class="flex-shrink-0 w-24 h-24 bg-gray-800 text-blue-400 rounded-full inline-flex items-center justify-center">
                      <svg fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round"
                          stroke-width="2" class="w-12 h-12" viewBox="0 0 24 24">
                          <circle cx="12" cy="12" r="9"></circle>
                          <path d="M9 15H15M12 12V6M12 12H6M12 12H18"></path>
                      </svg>

                  </div>
                  <div class="flex-grow sm:pl-6 mt-6 sm:mt-0">
                      <h2 class="font-medium title-font text-white mb-1 text-xl">Tentukan kategori dan masalah yang
                          ingin dibahas</h2>
                      <p class="leading-relaxed">Tentukan kategori dan masalah yang ingin dibahas pada sesi
                          beraspirasi. Isilah kolom yang tersedia pada web dengan informasi yang relevan dan jelas.
                      </p>
                  </div>
              </div>
          </div>
      </div>
      <div class="flex relative pb-10 sm:items-center md:w-2/3 mx-auto">
          <div class="h-full w-6 absolute inset-0 flex items-center justify-center">

          </div>
          <div class="rounded-lg bg-dark p-8 shadow-lg lg:col-span-3 lg:p-12">
              <div
                  class="flex-shrink-0 w-6 h-6 rounded-full mt-10 sm:mt-0 inline-flex items-center justify-center bg-blue-500 text-white relative z-10 title-font font-medium text-sm">
                  4</div>
              <div class="flex-grow md:pl-8 pl-6 flex sm:items-center items-start flex-col sm:flex-row">
                  <div
                      class="flex-shrink-0 w-24 h-24 bg-gray-800 text-blue-400 rounded-full inline-flex items-center justify-center">
                      <svg fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round"
                          stroke-width="2" class="w-12 h-12" viewBox="0 0 24 24">
                          <path d="M20 21v-2a4 4 0 00-4-4H8a4 4 0 00-4 4v2"></path>
                          <circle cx="12" cy="7" r="4"></circle>
                      </svg>
                  </div>
                  <div class="flex-grow sm:pl-6 mt-6 sm:mt-0">
                      <h2 class="font-medium title-font text-white mb-1 text-xl">Kirim formulir dan Tunggu balasan
                          atau konfirmasi</h2>
                      <p class="leading-relaxed">Setelah mengirimkan formulir, tunggulah balasan atau konfirmasi dari
                          pihak yang mengadakan sesi beraspirasi. Pastikan untuk memeriksa email atau nomor telepon
                          yang telah diisi pada formulir web untuk mengetahui informasi lebih lanjut tentang sesi
                          beraspirasi tersebut.</p>
                  </div>
              </div>
          </div>
      </div>
  </div>

</section>

<section class="text-gray-400 bg-gray-900 body-font">
  <div class="container px-5 py-24 mx-auto">
      <div class="flex flex-col text-center w-full mb-20">
          <h1 class="sm:text-3xl text-2xl font-medium title-font mb-4 text-white">Angka-Angka Membuktikan
              Keberhasilan Kami</h1>
          <p class="lg:w-2/3 mx-auto leading-relaxed text-base">Kami bangga dengan hasil yang telah dicapai dan terus
              meningkatkan kualitas layanan kami. Berikut adalah beberapa angka yang membuktikan keberhasilan kami:
          </p>
      </div>
      <div class="flex flex-wrap -m-4 text-center">
          <div class="p-4 md:w-1/4 sm:w-1/2 w-full">
              <div class="rounded-lg bg-dark p-8 shadow-lg lg:col-span-3 lg:p-12">
                  <svg fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round"
                      stroke-width="2" class="text-blue-400 w-12 h-12 mb-3 inline-block" viewBox="0 0 24 24">
                      <path d="M8 17l4 4 4-4m-4-5v9"></path>
                      <path d="M20.88 18.09A5 5 0 0018 9h-1.26A8 8 0 103 16.29"></path>
                  </svg>
                  <h2 class="title-font font-medium text-3xl text-white">2.7K</h2>
                  <p class="leading-relaxed">Downloads</p>
              </div>
          </div>
          <div class="p-4 md:w-1/4 sm:w-1/2 w-full">
              <div class="rounded-lg bg-dark p-8 shadow-lg lg:col-span-3 lg:p-12">
                  <svg fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round"
                      stroke-width="2" class="text-blue-400 w-12 h-12 mb-3 inline-block" viewBox="0 0 24 24">
                      <path d="M17 21v-2a4 4 0 00-4-4H5a4 4 0 00-4 4v2"></path>
                      <circle cx="9" cy="7" r="4"></circle>
                      <path d="M23 21v-2a4 4 0 00-3-3.87m-4-12a4 4 0 010 7.75"></path>
                  </svg>
                  <h2 class="title-font font-medium text-3xl text-white">1.3K</h2>
                  <p class="leading-relaxed">Users</p>
              </div>
          </div>
          <div class="p-4 md:w-1/4 sm:w-1/2 w-full">
              <div class="rounded-lg bg-dark p-8 shadow-lg lg:col-span-3 lg:p-12">
                  <svg fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round"
                      stroke-width="2" class="text-blue-400 w-12 h-12 mb-3 inline-block" viewBox="0 0 24 24">
                      <path d="M3 18v-6a9 9 0 0118 0v6"></path>
                      <path
                          d="M21 19a2 2 0 01-2 2h-1a2 2 0 01-2-2v-3a2 2 0 012-2h3zM3 19a2 2 0 002 2h1a2 2 0 002-2v-3a2 2 0 00-2-2H3z">
                      </path>
                  </svg>
                  <h2 class="title-font font-medium text-3xl text-white">74</h2>
                  <p class="leading-relaxed">Files</p>
              </div>
          </div>
          <div class="p-4 md:w-1/4 sm:w-1/2 w-full">
              <div class="rounded-lg bg-dark p-8 shadow-lg lg:col-span-3 lg:p-12">
                  <svg fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round"
                      stroke-width="2" class="text-blue-400 w-12 h-12 mb-3 inline-block" viewBox="0 0 24 24">
                      <path d="M12 22s8-4 8-10V5l-8-3-8 3v7c0 6 8 10 8 10z"></path>
                  </svg>
                  <h2 class="title-font font-medium text-3xl text-white">46</h2>
                  <p class="leading-relaxed">Places</p>
              </div>
          </div>
      </div>
  </div>
</section>

<section class="text-gray-400 bg-gray-900 body-font">
  <div class="container px-5 py-24 mx-auto">
      <div class="flex flex-col text-center w-full mb-20">
          <h1 class="text-2xl font-medium title-font mb-4 text-white tracking-widest">TIM KAMI</h1>
          <p class="lg:w-2/3 mx-auto leading-relaxed text-base">Kami adalah tim yang terdiri dari individu-individu
              berpengalaman dan berdedikasi dalam berbagai bidang. Kami bersama-sama bekerja untuk memberikan solusi
              aspirasi digital yang terbaik bagi Anda.</p>
      </div>
      <div class="flex flex-wrap -m-4">
          <div class="p-4 lg:w-1/2">
              <div
                  class="h-full flex sm:flex-row flex-col items-center sm:justify-start justify-center text-center sm:text-left">
                  <img alt="team"
                      class="flex-shrink-0 rounded-lg w-48 h-48 object-cover object-center sm:mb-0 mb-4"
                      src="https://dummyimage.com/200x200">
                  <div class="flex-grow sm:pl-8">
                      <h2 class="title-font font-medium text-lg text-white">Reiki Wibu</h2>
                      <h3 class="text-gray-500 mb-3">UI Developer</h3>
                      <p class="mb-4">DIY tote bag drinking vinegar cronut adaptogen squid fanny pack vaporware.
                      </p>
                      <span class="inline-flex">
                          <a class="text-gray-500">
                              <svg fill="none" stroke="currentColor" stroke-linecap="round"
                                  stroke-linejoin="round" stroke-width="2" class="w-5 h-5" viewBox="0 0 24 24">
                                  <path d="M18 2h-3a5 5 0 00-5 5v3H7v4h3v8h4v-8h3l1-4h-4V7a1 1 0 011-1h3z"></path>
                              </svg>
                          </a>
                          <a class="ml-2 text-gray-500">
                              <svg fill="none" stroke="currentColor" stroke-linecap="round"
                                  stroke-linejoin="round" stroke-width="2" class="w-5 h-5" viewBox="0 0 24 24">
                                  <path
                                      d="M23 3a10.9 10.9 0 01-3.14 1.53 4.48 4.48 0 00-7.86 3v1A10.66 10.66 0 013 4s-4 9 5 13a11.64 11.64 0 01-7 2c9 5 20 0 20-11.5a4.5 4.5 0 00-.08-.83A7.72 7.72 0 0023 3z">
                                  </path>
                              </svg>
                          </a>
                          <a class="ml-2 text-gray-500">
                              <svg fill="none" stroke="currentColor" stroke-linecap="round"
                                  stroke-linejoin="round" stroke-width="2" class="w-5 h-5" viewBox="0 0 24 24">
                                  <path
                                      d="M21 11.5a8.38 8.38 0 01-.9 3.8 8.5 8.5 0 01-7.6 4.7 8.38 8.38 0 01-3.8-.9L3 21l1.9-5.7a8.38 8.38 0 01-.9-3.8 8.5 8.5 0 014.7-7.6 8.38 8.38 0 013.8-.9h.5a8.48 8.48 0 018 8v.5z">
                                  </path>
                              </svg>
                          </a>
                      </span>
                  </div>
              </div>
          </div>
          <div class="p-4 lg:w-1/2">
              <div
                  class="h-full flex sm:flex-row flex-col items-center sm:justify-start justify-center text-center sm:text-left">
                  <img alt="team"
                      class="flex-shrink-0 rounded-lg w-48 h-48 object-cover object-center sm:mb-0 mb-4"
                      src="https://dummyimage.com/201x201">
                  <div class="flex-grow sm:pl-8">
                      <h2 class="title-font font-medium text-lg text-white">Kgs. Wahyu Hidayat</h2>
                      <h3 class="text-gray-500 mb-3">Front end</h3>
                      <p class="mb-4">DIY tote bag drinking vinegar cronut adaptogen squid fanny pack vaporware.
                      </p>
                      <span class="inline-flex">
                          <a class="text-gray-500">
                              <svg fill="none" stroke="currentColor" stroke-linecap="round"
                                  stroke-linejoin="round" stroke-width="2" class="w-5 h-5" viewBox="0 0 24 24">
                                  <path d="M18 2h-3a5 5 0 00-5 5v3H7v4h3v8h4v-8h3l1-4h-4V7a1 1 0 011-1h3z"></path>
                              </svg>
                          </a>
                          <a class="ml-2 text-gray-500">
                              <svg fill="none" stroke="currentColor" stroke-linecap="round"
                                  stroke-linejoin="round" stroke-width="2" class="w-5 h-5" viewBox="0 0 24 24">
                                  <path
                                      d="M23 3a10.9 10.9 0 01-3.14 1.53 4.48 4.48 0 00-7.86 3v1A10.66 10.66 0 013 4s-4 9 5 13a11.64 11.64 0 01-7 2c9 5 20 0 20-11.5a4.5 4.5 0 00-.08-.83A7.72 7.72 0 0023 3z">
                                  </path>
                              </svg>
                          </a>
                          <a class="ml-2 text-gray-500">
                              <svg fill="none" stroke="currentColor" stroke-linecap="round"
                                  stroke-linejoin="round" stroke-width="2" class="w-5 h-5" viewBox="0 0 24 24">
                                  <path
                                      d="M21 11.5a8.38 8.38 0 01-.9 3.8 8.5 8.5 0 01-7.6 4.7 8.38 8.38 0 01-3.8-.9L3 21l1.9-5.7a8.38 8.38 0 01-.9-3.8 8.5 8.5 0 014.7-7.6 8.38 8.38 0 013.8-.9h.5a8.48 8.48 0 018 8v.5z">
                                  </path>
                              </svg>
                          </a>
                      </span>
                  </div>
              </div>
          </div>

      </div>
  </div>
</section>
@endsection
