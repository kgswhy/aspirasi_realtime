@extends('Frontend.home.admin.layout.template')
@section('content')
<input type="text" id="basic-icon-default-uname" class="form-control dt-uname" placeholder="Web Developer" name="user-name" />
</div>
<div class="mb-1">
    <label class="form-label" for="basic-icon-default-email">Email</label>
    <input type="text" id="basic-icon-default-email" class="form-control dt-email" placeholder="john.doe@example.com" name="user-email" />
</div>
<div class="mb-1">
    <label class="form-label" for="basic-icon-default-contact">Contact</label>
    <input type="text" id="basic-icon-default-contact" class="form-control dt-contact" placeholder="+1 (609) 933-44-22" name="user-contact" />
</div>
<div class="mb-1">
    <label class="form-label" for="basic-icon-default-company">Company</label>
    <input type="text" id="basic-icon-default-company" class="form-control dt-contact" placeholder="PIXINVENT" name="user-company" />
</div>
<div class="mb-1">
    <label class="form-label" for="country">Country</label>
    <select id="country" class="select2 form-select">
        <option value="Australia">USA</option>
        <option value="Bangladesh">Bangladesh</option>
        <option value="Belarus">Belarus</option>
        <option value="Brazil">Brazil</option>
        <option value="Canada">Canada</option>
        <option value="China">China</option>
        <option value="France">France</option>
        <option value="Germany">Germany</option>
        <option value="India">India</option>
        <option value="Indonesia">Indonesia</option>
        <option value="Israel">Israel</option>
        <option value="Italy">Italy</option>
        <option value="Japan">Japan</option>
        <option value="Korea">Korea, Republic of</option>
        <option value="Mexico">Mexico</option>
        <option value="Philippines">Philippines</option>
        <option value="Russia">Russian Federation</option>
        <option value="South Africa">South Africa</option>
        <option value="Thailand">Thailand</option>
        <option value="Turkey">Turkey</option>
        <option value="Ukraine">Ukraine</option>
        <option value="United Arab Emirates">United Arab Emirates</option>
        <option value="United Kingdom">United Kingdom</option>
        <option value="United States">United States</option>
    </select>
</div>
<div class="mb-1">
    <label class="form-label" for="user-role">User Role</label>
    <select id="user-role" class="select2 form-select">
        <option value="subscriber">Subscriber</option>
        <option value="editor">Editor</option>
        <option value="maintainer">Maintainer</option>
        <option value="author">Author</option>
        <option value="admin">Admin</option>
    </select>
</div>
<div class="mb-2">
    <label class="form-label" for="user-plan">Select Plan</label>
    <select id="user-plan" class="select2 form-select">
        <option value="basic">Basic</option>
        <option value="enterprise">Enterprise</option>
        <option value="company">Company</option>
        <option value="team">Team</option>
    </select>
</div>
<button type="submit" class="btn btn-primary me-1 data-submit">Submit</button>
<button type="reset" class="btn btn-outline-secondary" data-bs-dismiss="modal">Cancel</button>
</div>
</form>
</div>
</div>
<!-- Modal to add new user Ends-->
</div>
<!-- list and filter end -->
</section>
<!-- users list ends -->

@endsection