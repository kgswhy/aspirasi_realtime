<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\DashboardController;

use App\Http\Controllers\UserController;
use App\Http\Controllers\ApiUserController;

use App\Http\Controllers\AdminController;

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/


Route::get('/test', function () {
    return view('test');
});

// Guest and User
Route::controller(UserController::class)->group(function(){
    Route::get('/home/contactus','ContactUs')->name('contact');
});

Route::middleware('auth')->group(function() {
    Route::controller(DashboardController::class)->group(function(){
        // Dashboard
        Route::get('/home','index')->name('dashboard');

        // Kelola Aspirasi
        Route::get('/admin/aspirasi/detail/{aspirasi}','detail')->name('aspirasi.detail');
        route::get('/admin/aspirasireport','AspirasiReport')->name('report');
        route::get('/admin/aspirasi/approve','AspirasiApprove')->name('approve');
        route::get('/admin/aspirasi/reject','AspirasiReject')->name('reject');

        // Kelola user
        route::get('/admin/setting/users','User')->name('user');
        route::get('/admin/setting/{id}/active','ActiveUser')->name('active');
        route::get('/admin/setting/{id}/deactive','DeactiveUser')->name('deactive');

        // Kelola admin
        route::get('/admin/setting','Setting')->name('settingadmin');
        Route::get('/logout','logout')->name('logout');
    });

    Route::controller(UserController::class)->group(function(){
        Route::get('/home/aspirasi','Aspirasi')->name('aspirasi');
        Route::get('/home/aspirasi/list','StatusAspirasi')->name('status');
        Route::get('/home/account','Account')->name('Account');
        Route::get('/home/setting','Setting')->name('setting');
    });


    //Api User
    Route::controller(ApiUserController::class)->group(function() {
        Route::post('/api/input', 'input_aspirasi')->name('api.aspirasi.input');
    });

    // Api admin
    Route::controller(AdminController::class)->group(function() {
        Route::get('/admin/aspirasi', 'aspirasi')->name('aspirasi.all');
    });

    route::controller(AspirasiController::class)->group(function(){
        route::get('/home/all-Aspirasi','index')->name('allaspirasi');
        route::get('/home/add-aspirasi','AddAspirasi')->name('addaspirasi');
        route::get('/home/store-aspirasi','StoreAspirasi')->name('storeaspirasi');
        route::get('/home/edit-aspirasi','EditAspirasi')->name('editaspirasi');
        route::post('/home/update-aspirasi','UpdateAspirasi')->name('UpdateAspirasi');
        route::get('/home/delete-aspirasi','DeleteAspirasi')->name('deleteaspirasi');
        
    });
});



Route::controller(AuthController::class)->group(function() {
    /* get method */
    Route::get('/login','LoginPage')->name('login_get');
    Route::get('/registerpage','RegisterPage')->name('register');
    Route::get('/forgotpassword','ForgotPassword')->name('forgot');
    Route::get('/','HomePage')->name('home');

    /* post method */
    Route::post('/login', 'login')->name('login_post');
});

