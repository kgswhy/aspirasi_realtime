<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\AdminController;

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/


Route::get('/test', function () {
    return view('test');
});

Route::middleware('auth')->group(function() {
    Route::controller(DashboardController::class)->group(function(){
        Route::get('/home','index')->name('dashboard');
        Route::get('/logout','logout')->name('logout');
    });
    Route::controller(UserController::class)->group(function(){
        Route::get('/home/aspirasi','Aspirasi')->name('aspirasi');
        Route::get('/home/contactus','ContactUs')->name('contact');
        Route::get('/home/statusaspirasi','StatusAspirasi')->name('status');
    });
    
});

<<<<<<< HEAD

Route::controller(AuthController::class)->group(function() {
=======
Route::controller(AuthController::class)->group(function(){
>>>>>>> 18f3a0a57037b7f38cb16e5edecb591521b43464
    Route::get('/login','LoginPage')->name('login_get');
    Route::get('/registerpage','RegisterPage')->name('register');
    Route::get('/forgotpassword','ForgotPassword')->name('forgot');
    Route::get('/','HomePage')->name('home');

    Route::post('/login', 'login')->name('login_post');
});

Route::controller(AdminController::class)->group(function() {
    Route::get('/admin/aspirasi', 'aspirasi')->name('aspirasi.all');
});