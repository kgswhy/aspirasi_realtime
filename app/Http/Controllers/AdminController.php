<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Aspirasi;

class AdminController extends Controller
{
    //

    private function tooLong($string, $maxLength = 5) {
        
        return strlen($string) > $maxLength ? trim(substr($string, 0, $maxLength))."..." : $string;

    }
    public function aspirasi(Request $request) {

        if(auth()->user()->role == 1) { 
            $aspirasi = Aspirasi::all()->toArray();
            
            $return_data = [];

            foreach($aspirasi as $data_aspirasi) {            
                $add_data = [ 
                    "nis" => $data_aspirasi['nis'],
                    "email" => $this->tooLong($data_aspirasi['user']['email'], 23),
                    "name" => $this->tooLong($data_aspirasi['user']['name'], 15),

                    "aspirasi" => [
                        "id" => $data_aspirasi['aspirasi_id'],
                        "status" => $data_aspirasi['status'],
                        "pesan" => @$data_aspirasi['pesan'] ? $this->tooLong($data_aspirasi['pesan'], 7) : "Tidak ada pesan",
                        "submit_at" => $data_aspirasi['created_at'],
                        "resolved_at" => @$data_aspirasi['resolved_at'] ? $data_aspirasi['resolved_at'] : "Belum Selesai",
                    ]

                ];

                array_push($return_data, $add_data);
            }

            return response()->json([
                    'success' => true,
                    'data' => $return_data
            ], 200);
        }
    }
}
