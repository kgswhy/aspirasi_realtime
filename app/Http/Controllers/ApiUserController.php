<?php

namespace App\Http\Controllers;

use Illuminate\Http\Response;
use Illuminate\Http\Request;

use App\Models\Aspirasi;
use App\Models\jenis_kategori;
use App\Models\tujuan_aspirasi;

use Illuminate\Database\Eloquent\ModelNotFoundException;

class ApiUserController extends Controller
{
    

    private function response_builder($error_code = 0, $message = '') {
        /*
        * Error code 0 = Aman aja
        * Error code 1 = Code jalan tapi goals tidak tercapai
        * Error code 2 = Server error / Code gajalan
        */

    
        $return['error'] = $error_code;
        $return['message'] = $message;

        header('Content-Type: application/json; charset=utf-8');
        exit(json_encode($return));

    }

    private function kategori_tujuan($id_kategori, $id_tujuan) {
        
        try {

            jenis_kategori::select('jenis_kategori')->where('id_kategori', '=', $id_kategori)->firstOrFail()->toArray();
            tujuan_aspirasi::select('tujuan')->where('id_tujuan', '=', $id_tujuan)->firstOrFail()->toArray();
            return true;

        } catch (ModelNotFoundException $e) {
            $this->response_builder(1, 'Jenis atau tujuan tidak ada');
        }

    }

    public function input_aspirasi(Request $request) {
        
        $validated = $request->validate([
            'jenis_aspirasi' => 'required|max:3',
            'judul_aspirasi' => 'required',
            'tujuan_aspirasi' => 'required',
            'pesan_aspirasi' => 'required',
        ]);

        $check = $this->kategori_tujuan($request->jenis_aspirasi, $request->tujuan_aspirasi);

        $bukti_image = $request->file('bukti');
        $filename = null;

        if($bukti_image) {
            $request->validate(['bukti' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048']);
            $filename =  md5(uniqid(rand(), true)).".".$bukti_image->extension();
            $bukti_image->move(public_path('bukti_aspirasi/'), $filename);
        }

        Aspirasi::create([
            'judul' => $request->judul_aspirasi,
            'nis' => auth()->user()->nis, 
            'aspirasi_id' => $this->generateAspirasiId(), 
            'kategori_id' => $request->jenis_aspirasi,
            'tujuan_id' => $request->tujuan_aspirasi,
            'pesan' => $request->pesan_aspirasi,
            'bukti' => $filename
        ]);

        $this->response_builder(0, 'Aspirasi berhasil di submit!');
    }

    public function generateAspirasiId()
    {
        do {
            $code = random_int(1000000, 9999999);
        } while (Aspirasi::where("aspirasi_id", "=", $code)->first());
  
        return $code;
    }
}
