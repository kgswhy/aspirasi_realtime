<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Aspirasi;
use App\Models\User;

class DashboardController extends Controller
{
    public function index() {
       return view('Frontend.home.index', [
            'user' => auth()->user(),
            'role' => auth()->user()->role == 0 ? "User" : "Admin"
        ]);
    }

    public function Setting(){
        return view('Frontend.home.admin.setting.settingAdmin',[
            'user' => auth()->user(),
            'role' => auth()->user()->role == 0 ? "User" : "Admin"
        ]);
    }
    public function User(){
        return view('Frontend.home.admin.setting.settingUsers', [
            'user' => auth()->user(),
            'role' => auth()->user()->role == 0 ? "User" : "Admin"
        ]);
    }

    public function logout() {
        auth()->logout();

        return redirect()->route('login_get');
    }
}
