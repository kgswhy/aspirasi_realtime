<?php

namespace App\Http\Controllers;

use Illuminate\Auth\Events\Login;
use Illuminate\Http\Request;

class AuthController extends Controller
{

    /* 
    *
    * Handle the views from here
    *
    */

    public function LoginPage(){
        if(auth()->check()) {
            return redirect()->route('dashboard');
        } else {
            return view('Frontend.auth.login');
        }
    }

    public function RegisterPage(){
        return view('Frontend.auth.regis');
    }
    
    public function ForgotPassword(){
        return view('Frontend.auth.forgot');
    }

    public function HomePage(){
        return view('Frontend.home.page.index');
    }

    public function Aspirasi(){
        return view('Frontend.home.user.aspirasi');
    }
    /* 
    *
    * Handle the postdata from here
    *
    */

    public function login(Request $request) {
        
        if(auth()->attempt($request->only(['email', 'password']))) {
            return response()->json([
                'success' => true,
                'redirect_url' => route('dashboard')
            ], 200);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Email atau password salah!'
            ], 200);
        }
    }

    public function logout() {
        auth()->logout();

        return redirect(route('login_get'));
    }
}
