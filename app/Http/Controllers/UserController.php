<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Aspirasi;
use App\Models\jenis_kategori;
use App\Models\tujuan_aspirasi;


class UserController extends Controller
{
    public function Aspirasi() {
        return view('Frontend.home.user.aspirasi', [
            'user' => auth()->user(),
            'jenis_kategori' => jenis_kategori::All()->toArray(),
            'tujuan_aspirasi' => tujuan_aspirasi::All()->toArray()
        ]);
    }

    public function index() {
        return view('Frontend.home.user.home', [
            'user' => auth()->user()
        ]);
    }

    public function ContactUs() {
        return view('Frontend.home.user.Contact', [
            'user' => auth()->user()
        ]);
    }

    public function StatusAspirasi() {

        $user = auth()->user();
        $user_aspirasi = Aspirasi::where('nis', '=', $user->nis)->get()->toArray();
     

        return view('Frontend.home.user.status', [
            'user' => $user,
            'list_aspirasi' => $user_aspirasi
        ]);
    }

    public function Account(){
        return view('Frontend.home.user.account',[
            'user' => auth()->user()
        ]);
    }

    public function Setting(){
        return view('Frontend.home.user.setting',[
            'user' => auth()->user()
        ]);
    }

}
