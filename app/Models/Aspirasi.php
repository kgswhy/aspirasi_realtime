<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Models\User;

class Aspirasi extends Model
{
    use HasFactory;
    public $fillable = ['judul', 'nis', 'aspirasi_id', 'kategori_id', 'tujuan_id', 'pesan', 'bukti'];

    protected $with = ["user"];

    public function user() {
        return $this->belongsTo(User::class, 'nis', 'nis');
    }
}
