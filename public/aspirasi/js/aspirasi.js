$(document).ready(function() {

    $(".send-aspirasi").click(function(e) {

        e.preventDefault();

        var aspirasi_data = new FormData($(".aspirasi-form")[0]);

        $.ajax({
            url: routes.aspirasi_input,
            type: 'POST',
            data: aspirasi_data,

            success: function (data) {

                if(data.error == 0) {
                    Swal.fire({
                        type: 'success',
                        title: 'Berhasil!',
                        text: data.message,
                    });
                } else if(data.error == 1) {
                    Swal.fire({
                        type: 'error',
                        title: 'Gagal!',
                        text: data.message
                    });
                }
            },

            error: function(response){

                Swal.fire({
                    type: 'error',
                    title: 'Opps!',
                    text: 'server error!'
                });

            },
            
            cache: false,
            contentType: false,
            processData: false
        });

    })

});
