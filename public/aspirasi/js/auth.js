$(document).ready(function() {
    
    
    $(".login-button").click(function(e) {

        e.preventDefault();
        function showSuccessModal() {
            $('#login-modal').removeClass('hidden');
        }
        let email = $("#email").val();
        let password = $("#password").val();
        let token = $("meta[name='csrf-token']").attr("content");

        if(email.trim() == "") {

            Swal.fire({
                type: 'warning',
                title: 'Oops...',
                text: 'Kamu belum mengisi email kamu!'
            });

        } else if(password.trim() == "") {

            Swal.fire({
                type: 'warning',
                title: 'Oops...',
                text: 'Kamu belum mengisi password kamu!'
            });

        } else {

            $.ajax({

                url: routes.login,
                type: "POST",
                dataType: "JSON",
                cache: false,
                data: {
                    "email": email,
                    "password": password,
                    "_token": token
                },

                success:function(response){

                    if (response.success) {

                        Swal.fire({
                            type: 'success',
                            title: 'Login Berhasil!',
                            text: 'Anda akan di arahkan dalam 3 Detik',
                            timer: 3000,
                            showCancelButton: false,
                            showConfirmButton: false
                        }).then (function() {
                            window.location.href = response.redirect_url;
                        });

                    } else {

                        console.log(response.success);

                        Swal.fire({
                            type: 'error',
                            title: 'Login Gagal!',
                            text: response.message
                        });

                    }

                    console.log(response);

                },

                error:function(response){

                    Swal.fire({
                        type: 'error',
                        title: 'Opps!',
                        text: 'server error!'
                    });

                    console.log(response);

                }

            });

        }

    })

})
