$(document).ready(function() {

    var aspirasiPreview = '/home/aspirasi/';

    $.ajax({
        dataType: "JSON",
        url: routes.aspirasi_list,
        success: function(data, textStatus, jqXHR) {
            console.log(data.data);
            $("#aspirasi").DataTable({
                data: data.data,
                autoWidth: false,
                columns: [
                    // columns according to JSON
                    {
                        data: 'id'
                    },
                    {
                        data: 'name'
                    }
                ],
                columnDefs: [
                    {
                        // aspirasi id
                        responsivePriority: 2,
                        targets: 0,
                        width: '46px',
                        render: function(data, type, full, meta) {
                            var $aspirasi_id = full['aspirasi']['id'];
                            // Creates full output for row
                            var $rowOutput = '<a class="fw-bold" href="' + aspirasiPreview + '"> #' + $aspirasi_id + '</a>';
                            return $rowOutput;
                        }
                    },
                    {
                        // Client name and Service
                        targets: 1,
                        responsivePriority: 4,
                        width: '270px',
                        render: function(data, type, full, meta) {
                            var $name = full['name'],
                                $email = full['email'],
                                $image = full['avatar'],
                                stateNum = Math.floor(Math.random() * 6),
                                states = ['success', 'danger', 'warning', 'info', 'primary', 'secondary'],
                                $state = states[stateNum],
                                $initials = $name.match(/\b\w/g) || [];
                            $initials = (($initials.shift() || '') + ($initials.pop() || '')).toUpperCase();

                            if ($image) {
                                // For Avatar image
                                var $output =
                                    '<img  src="' + assetPath + 'images/avatars/' + $image + '" alt="Avatar" width="32" height="32">';
                            } else {
                                // For Avatar badge
                                $output = '<div class="avatar-content">' + $initials + '</div>';
                            }
                            // Creates full output for row
                            var colorClass = $image === '' ? ' bg-light-' + $state + ' ' : ' ';

                            var $rowOutput =
                                '<div class="d-flex justify-content-left align-items-center">' +
                                '<div class="avatar-wrapper">' +
                                '<div class="avatar' +
                                colorClass +
                                'me-50">' +
                                $output +
                                '</div>' +
                                '</div>' +
                                '<div class="d-flex flex-column">' +
                                '<h6 class="user-name text-truncate mb-0">' +
                                $name +
                                '</h6>' +
                                '<small class="text-truncate text-muted">' +
                                $email +
                                '</small>' +
                                '</div>' +
                                '</div>';
                            return $rowOutput;
                        }
                    },
                    {
                        // Tanggal aspirasi masuk
                        targets: 2,
                        width: '130px',
                        render: function (data, type, full, meta) {
                            moment.locale('id')
                            var formatted = moment(full['aspirasi']['submit_at']).format('DD MMM YYYY h:mm a')

                          var $rowOutput = formatted
                          return $rowOutput;
                        }
                    },
                    {
                        // Tanggal aspirasi selesai
                        targets: 3,
                        width: '130px',
                        render: function (data, type, full, meta) {

                            var $tanggal_selesai = full['aspirasi']['resolved_at'];
                            moment.locale('id')

                            let display_tanggal_selesai = $tanggal_selesai == "Belum Selesai" ? "Belum Selesai" : moment(full['aspirasi']['resolved_at']).format('DD MMM YYYY h:mm a')
                            return display_tanggal_selesai;
                        }
                    },
                    {
                        // Pesan
                        targets: 4,
                        width: '130px',
                        render: function (data, type, full, meta) {
                            return full['aspirasi']['pesan'];
                        }
                    },
                    {
                        // Status
                        targets: 5,
                        width: '98px',
                        render: function (data, type, full, meta) {
                          var $status = full['aspirasi']['status'];

                          if($status == 'Menunggu') {
                            var $badge_class = "badge-light-danger";
                          } else if($status == "Proses") {
                            var $badge_class = "badge-light-warning";
                          } else if($status == "Selesai") {
                            var $badge_class = "badge-light-success";
                          }
                            return '<span class="badge rounded-pill ' + $badge_class + '" text-capitalized> ' + $status + ' </span>';
                        }
                    },
                    {
                        // Actions
                        targets: 6,
                        title: 'Actions',
                        width: '80px',
                        orderable: false,
                        render: function (data, type, full, meta) {
                          return (
                            '<div class="dt-center">' +
                            '<a class="me-25" href="' + routes.aspirasi_detail + '/' + full['aspirasi']['id'] + '" data-bs-toggle="tooltip" data-bs-placement="top" title="Lihat detail">' +
                            feather.icons['eye'].toSvg({ class: 'font-medium-2 text-body' }) +
                            '</a>' +
                            '</div>'
                          );
                        },
                        class: "text-center"
                      }

                ],
                dom:
                    '<"row d-flex justify-content-between align-items-center m-1"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
            
            })
        }
    })

});